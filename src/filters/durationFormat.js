import Vue from 'vue'
import moment from 'moment'

Vue.filter('formatDuration', function(value) {
	if (value) {
		const duration = moment.duration(value)
		const seconds = duration.seconds() < 10 ? `0${duration.seconds()}` : `${duration.seconds()}`
		return `${duration.minutes()}:${seconds}`
	}
})
