import config from '../config'

export default {
	get(url, params) {
		let finalUrl = `${config.API_URL}${url}`

		if (params && Object.keys(params)) {
			const finalParams = Object.keys(params).map((key) => `${key}=${params[key]}`)
			finalUrl += `?${finalParams.join('&')}`
		}

		return fetch(finalUrl, {
			method: 'GET',
			headers: new Headers(),
		}).then((response) => {
			return response.json()
		})
	},
	post(url, params) {
		const header = new Headers()
		header.append('Content-Type', 'application/json')
		header.append('Accept', 'application/json')
		let finalUrl = `${config.API_URL}${url}`
		return fetch(finalUrl, {
			method: 'POST',
			headers: header,
			body: JSON.stringify(params)
		}).then((response) => {
			return response.json()
		})
	}
}