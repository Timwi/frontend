import api from './api'
import spotifyApi from './spotifyApi'

export default {
	getPaginated(params) {
		return spotifyApi.get('/search', params)
	},
	addTrackToLibrary(trackid) {
		return api.post('/track', {id: trackid})
	},
	favTrack(trackid, state) {
		return api.post('/track/fav', {id: trackid, state})
	},
	saveTag(trackids, tagname) {
		return api.post('/tag', {name: tagname, trackids})
	},
	removeTrackFromLibrary(trackid) {
		return api.post('/track/remove', {id: trackid})
	},
	getTracksOfLibrary() {
		return api.get('/tracks')
	}
}