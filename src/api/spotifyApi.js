import config from '../config'

export default {
	get(url, params) {
		let finalUrl = `${config.SPOTIFY_URL}${url}`
		if (Object.keys(params)) {
			const finalParams = Object.keys(params).map((key) => `${key}=${params[key]}`)
			finalUrl += `?${finalParams.join('&')}`
		}

		const accessToken = localStorage.getItem('token')

		if (accessToken) {
			const header = new Headers()
			header.append('Authorization', 'Bearer ' + accessToken)
	
			return fetch(finalUrl, {
				method: 'GET',
				headers: header,
			}).then((response) => {
				return response.json()
			}).catch(() => {
				return Promise.reject('Merci de vous authentifier')
			})
		} else {
			return Promise.reject('Merci de vous authentifier')
		}
	},
	auth() {
		fetch(`${config.API_URL}/token`, {method: 'GET', headers: new Headers()}).then((res) => {
			if (res.status !== 200) {
				console.error('Problème d\'authentification', res)
			} else {
				res.json().then((data) => {
					localStorage.setItem('token', data.token)
				})
			}
		}).catch((err) => {
			console.error('Fetch error', err)
		})
	}
}