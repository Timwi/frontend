import TrackApi from '../api/trackApi'
import SpotifyApi from '../api/spotifyApi'

/**
 * Modèle de définition des musiques
 */
class TrackModel {
	constructor(track) {
		this.id = track.id
		this.name = track.name
		this.duration = track.duration_ms
		this.release_date = track.album.release_date
		this.image_url = track.album.images[0].url
		this.tags = track.tags || []
		this.fav = track.fav || false
		this.isSelect = false
	}

	/**
	 * Permet de récupérer une liste paginé sur spotify
	 * @param {Object} params 
	 * @param {Integer} params.limit	Nombre de résultat à récupérer 
	 * @param {Integer} params.page	Page à récupérer
	 * @param {String} params.query
	 */
	static getPaginated(params) {
		const formatedParams = {}
		formatedParams.limit = params.nbItems
		formatedParams.offset = params.nbItems * (params.page - 1)
		formatedParams.q = params.query
		formatedParams.type = 'track'
		return TrackApi.getPaginated(formatedParams).then((data) => {
			if (data && data.tracks && data.tracks.items) {
				return {
					data: data.tracks.items.map((item => new TrackModel(item))),
					total: data.tracks.total
				}
			}
			return {
				data: [],
				total: 0
			}
		})
	}

	/**
	 * Permet d'ajouter une musique à la bibliothèque
	 * @param {Object} track 
	 */
	static addTrackToLibrary(track) {
		return TrackApi.addTrackToLibrary(track.id)
	}

	/**
	 * Permet de supprimer une musique de la bibliothèque
	 * @param {Object} track 
	 */
	static removeTrackFromLibrary(track) {
		return TrackApi.removeTrackFromLibrary(track.id)
	}

	/**
	 * Permet de passer une musique en favoris
	 * @param {Object} track 
	 */
	static favTrack(track) {
		return TrackApi.favTrack(track.id, !track.fav)
	}

	/**
	 * Permet d'ajouter un tag sur plusieurs musique de la bibliothèque
	 * @param {Array<Object>} tracks 
	 * @param {String} tagName 
	 */
	static saveTag(tracks, tagName) {
		return TrackApi.saveTag(tracks.map(track => track.id), tagName)
	}

	/**
	 * Récupère les musiques de la bibliothèque
	 */
	static getTracksOfLibrary() {
		return TrackApi.getTracksOfLibrary().then((data) => {
			return data.tracks
		})
	}

	/**
	 * Récupère les musiques de la bibliothèque et les info spotify associés
	 */
	static getTracksOfLibraryWithData() {
		return TrackModel.getTracksOfLibrary().then((tracks) => {
			if (tracks.length) {
				return SpotifyApi.get('/tracks', {ids: tracks.map(track => track.id).join(',')}).then((data) => {
					if (data && data.tracks) {
						return {
							data: data.tracks.map(((item) => {
								const databaseTrack = tracks.find(track => track.id === item.id)
								if (databaseTrack) {
									Object.assign(item, databaseTrack)
								}
								return new TrackModel(item)
							})),
							total: data.tracks.length
						}
					}
					return {
						data: [],
						total: 0
					}
				})						
			}
			return {
				data: [],
				total: 0
			}
		})
	}
}

export {
	TrackModel
}