import Vue from 'vue'
import App from './app.vue'
import router from './router'

// Style
import '../node_modules/bootstrap/scss/bootstrap.scss'

// Format
import './filters/durationFormat'
import './filters/dateFormat'


new Vue({
	el: '#app',
	router,
	render: h => h(App),
})