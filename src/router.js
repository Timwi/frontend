import Vue from 'vue'
import Router from 'vue-router'

import Home from './view/home.vue'
import Search from './view/search.vue'
import Library from './view/library.vue'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/search',
			name: 'search',
			component: Search
		},
		{
			path: '/library',
			name: 'library',
			component: Library
		}
	]
})